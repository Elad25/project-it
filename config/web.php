<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'maSyCVqTNq5xr4MDo9EEgh7N-KsJmPB9',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
		'authManager' => [
            'class' => 'yii\rbac\DbManager',
			],
     'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,	
			'rules' => [
				'users' => 'user/index',
				'user/<id:\d+>' => 'user/view',
				'user/update/<id:\d+>' => 'user/update',
				'user/delete/<id:\d+>' => 'user/delete',

				'teachers' => 'teacher/index',
				'teacher/<id:\d+>' => 'teacher/view',
				'teacher/update/<id:\d+>' => 'teacher/update',
				'teacher/delete/<id:\d+>' => 'teacher/delete',				
				
				'courses' => 'course/index',
				'course/<id:\d+>' => 'course/view',
				'course/update/<id:\d+>' => 'course/update',
				'course/delete/<id:\d+>' => 'course/delete',	
				
				'secretarys' => 'secretary/index',
				'secretary/<id:\d+>' => 'secretary/view',
				'secretary/update/<id:\d+>' => 'secretary/update',
				'secretary/delete/<id:\d+>' => 'secretary/delete',

				'principals' => 'principal/index',
				'principal/<id:\d+>' => 'principal/view',
				'principal/update/<id:\d+>' => 'principal/update',
				'principal/delete/<id:\d+>' => 'principal/delete',	

				'principals' => 'principal/index',
				'principal/<id:\d+>' => 'principal/view',
				'principal/update/<id:\d+>' => 'principal/update',
				'principal/delete/<id:\d+>' => 'principal/delete',	

				'classnames' => 'classname/index',
				'classname/<id:\d+>' => 'classname/view',
				'classname/update/<id:\d+>' => 'classname/update',
				'classname/delete/<id:\d+>' => 'classname/delete',			

				'students' => 'student/index',
				'student/<id:\d+>' => 'student/view',
				'student/update/<id:\d+>' => 'student/update',
				'student/delete/<id:\d+>' => 'student/delete',	

				'classnames' => 'classname/index',
				'classname/<id:\d+>' => 'classname/view',
				'classname/update/<id:\d+>' => 'classname/update',
				'classname/delete/<id:\d+>' => 'classname/delete',					
				
			],
		],
    ],
    'params' => $params,
];


if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;

<?php

namespace app\controllers;

use Yii;
use app\models\StudentTeacher;
use app\models\StudentTeacherSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * StudentTeacherController implements the CRUD actions for StudentTeacher model.
 */
class StudentTeacherController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentTeacher models.
     * @return mixed
     */
    public function actionIndex()
    {
        //access control
		if (!\Yii::$app->user->can('indexStudentTeacher')) // only team members can watch student-teacher 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to watch student-teacher');
		$searchModel = new StudentTeacherSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'nameOfTeachers' => StudentTeacher::getTechersByAllNames(),    ///////////////////////////// necessary for dropdown 
			'nameOfTeacher' => $searchModel->teacherId,			       ///////////////////////////// necessary for dropdown 
			'nameOfStudents' => StudentTeacher::getStudentNameWithAllNames(),    ///////////////////////////// necessary for dropdown 
			'nameOfStudent' => $searchModel->studentId,			       ///////////////////////////// necessary for dropdown 
			
		]);
    }

    /**
     * Displays a single StudentTeacher model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can view student-teacher 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to view student-teacher');
		return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StudentTeacher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can create student-teacher 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to create student-teacher');
		$model = new StudentTeacher();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StudentTeacher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can update student-teacher 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to update student-teacher');
		$model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StudentTeacher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can delete student-teacher 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to delete student-teacher');
		$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StudentTeacher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentTeacher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentTeacher::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

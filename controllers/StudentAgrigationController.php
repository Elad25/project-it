<?php

namespace app\controllers;

use Yii;
use app\models\StudentAgrigation;
use app\models\StudentAgrigationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * StudentAgrigationController implements the CRUD actions for StudentAgrigation model.
 */
class StudentAgrigationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentAgrigation models.
     * @return mixed
     */
    public function actionIndex()
    {
        //access control
		if (!\Yii::$app->user->can('indexStudentAgrigations')) // only team members can watch course-class-student 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to watch course-class-student');
		$searchModel = new StudentAgrigationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StudentAgrigation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can view course-class-student
				throw new UnauthorizedHttpException ('Hey, You are not allowed to view course-class-student');
		return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StudentAgrigation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can create course-class-student
				throw new UnauthorizedHttpException ('Hey, You are not allowed to create course-class-student');
		$model = new StudentAgrigation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StudentAgrigation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can update course-class-student
				throw new UnauthorizedHttpException ('Hey, You are not allowed to update course-class-student');
		$model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StudentAgrigation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can delete course-class-student
				throw new UnauthorizedHttpException ('Hey, You are not allowed to delete course-class-student');
		$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StudentAgrigation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentAgrigation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentAgrigation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Userrole;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        //access control
		if (!\Yii::$app->user->can('indexUser')) // only team members can watch users 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to watch users');
		$searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'userRoles' => Userrole::getUserRoleWithAllRoles(),    ///////////////////////////// necessary for dropdown 
			'userRole' => $searchModel->userRole,			       ///////////////////////////// necessary for dropdown 
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //access control
		if (!\Yii::$app->user->can('viewUser')) // only secretary's and principals can view users 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to view users');
		return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		//access control
		if (!\Yii::$app->user->can('createUser')) // only principal can create new users 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to create new users');
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {                          ////////////////////////// necessary for user view 
			$roles = User::getRoles(); 
            return $this->render('create', [
                'model' => $model,
				'roles' => $roles,  
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) /////////////   we need access control after we implement the rule !!!
    {
        //access control
		if (!\Yii::$app->user->can('updateUser')) // only secretary's and principals can update users 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to update new users');
		$model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else { ////////////////////////// necessary for user view
			$roles = User::getRoles(); 
            return $this->render('create', [
                'model' => $model,
				'roles' => $roles,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //access control
		if (!\Yii::$app->user->can('deleteUser')) // only principal can delete users 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to delete users');
		$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

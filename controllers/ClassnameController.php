<?php

namespace app\controllers;

use Yii;
use app\models\Classname;
use app\models\ClassnameSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * ClassnameController implements the CRUD actions for Classname model.
 */
class ClassnameController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Classname models.
     * @return mixed
     */
    public function actionIndex()
    {
        //access control
		if (!\Yii::$app->user->can('indexClassName')) // only team members can watch class's 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to watch class');
		$searchModel = new ClassnameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Classname model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can view class's
				throw new UnauthorizedHttpException ('Hey, You are not allowed to view class');
		return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Classname model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can create class's
				throw new UnauthorizedHttpException ('Hey, You are not allowed to create class');
		$model = new Classname();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->classNumber]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Classname model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can update class's
				throw new UnauthorizedHttpException ('Hey, You are not allowed to update class');
		$model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->classNumber]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Classname model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can delete class's
				throw new UnauthorizedHttpException ('Hey, You are not allowed to delete class');
		$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Classname model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Classname the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Classname::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

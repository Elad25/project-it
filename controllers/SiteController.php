<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
	
	//////////////////////////////////////////////////// RBAC  /////////////////////////////////////////
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$basic = $auth->createRole('basic');
		$auth->add($basic);
		
		$pro = $auth->createRole('pro');
		$auth->add($pro);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

	public function actionBasicpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$indexUser = $auth->createPermission('indexUser');
		$indexUser->description = 'All users can view users';
		$auth->add($indexUser);
		
		$updateOwnUser = $auth->createPermission('updateOwnUser'); 
		$updateOwnUser->description = 'Users can update own user';
		$auth->add($updateOwnUser);	

		$updateOwnPassword = $auth->createPermission('updateOwnPassword'); 
		$updateOwnPassword->description = 'Users can update own password';
		$auth->add($updateOwnPassword);			
		
		$indexStudentTeacher = $auth->createPermission('indexStudentTeacher');
		$indexStudentTeacher->description = 'All users can view student-teachers';
		$auth->add($indexStudentTeacher);
		
		$indexClassName = $auth->createPermission('indexClassName');
		$indexClassName->description = 'All users can view class names';
		$auth->add($indexClassName);
		
		$indexCourse = $auth->createPermission('indexCourse');
		$indexCourse->description = 'All users can view course';
		$auth->add($indexCourse);
		
		$indexCourseClass = $auth->createPermission('indexCourseClass');
		$indexCourseClass->description = 'All users can view course-class';
		$auth->add($indexCourseClass);

		$indexStudentAgrigations = $auth->createPermission('indexStudentAgrigations');
		$indexStudentAgrigations->description = 'All users can view Student Agrigations';
		$auth->add($indexStudentAgrigations);
		
		$indexTeacher = $auth->createPermission('indexTeacher');
		$indexTeacher->description = 'All users can view teachers';
		$auth->add($indexTeacher);
	}


	public function actionPropermissions()
	{
		$auth = Yii::$app->authManager;

		$updateUser = $auth->createPermission('updateUser'); 
		$updateUser->description = 'pro can update users';
		$auth->add($updateUser);	

		$viewUser = $auth->createPermission('viewUser'); 
		$viewUser->description = 'pro can view users';
		$auth->add($viewUser);	

		$crudWithoutIndex = $auth->createPermission('crudWithoutIndex'); 
		$crudWithoutIndex->description = 'pro can do crud without index ';
		$auth->add($crudWithoutIndex);

		$fullCrudPro = $auth->createPermission('fullCrudPro'); 
		$fullCrudPro->description = 'Pro can do full crud ';
		$auth->add($fullCrudPro);		
	}
	

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$updatePassword = $auth->createPermission('updatePassword'); 
		$updatePassword->description = 'admin can update all users passwords';
		$auth->add($updatePassword);

		$deleteUser = $auth->createPermission('deleteUser'); 
		$deleteUser->description = 'admin can delete users';
		$auth->add($deleteUser);

		$createUser = $auth->createPermission('createUser');  
		$createUser->description = 'admin can create all users';
		$auth->add($createUser);
		
		
		$fullCrudAdmin = $auth->createPermission('fullCrudAdmin'); 
		$fullCrudAdmin->description = 'admin can do full crud to principals';
		$auth->add($fullCrudAdmin);
	
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$basic = $auth->getRole('basic'); 

		$indexUser = $auth->getPermission('indexUser'); 
		$auth->addChild($basic, $indexUser);
		
		$indexTeacher = $auth->getPermission('indexTeacher'); 
		$auth->addChild($basic, $indexTeacher);		
		
		$updateOwnUser = $auth->getPermission('updateOwnUser'); 
		$auth->addChild($basic, $updateOwnUser);

		$updateOwnPassword = $auth->getPermission('updateOwnPassword'); 
		$auth->addChild($basic, $updateOwnPassword);

		$indexStudentAgrigations = $auth->getPermission('indexStudentAgrigations'); 
		$auth->addChild($basic, $indexStudentAgrigations);

		$indexClassName = $auth->getPermission('indexClassName'); 
		$auth->addChild($basic, $indexClassName);

		$indexCourse = $auth->getPermission('indexCourse'); 
		$auth->addChild($basic, $indexCourse);

		$indexCourseClass = $auth->getPermission('indexCourseClass'); 
		$auth->addChild($basic, $indexCourseClass);	

		$indexStudentTeacher = $auth->getPermission('indexStudentTeacher'); 
		$auth->addChild($basic, $indexStudentTeacher);		
		
		////////////////////////////////////
		
		$pro = $auth->getRole('pro'); 
		$auth->addChild($pro, $basic);
		
		$updateUser = $auth->getPermission('updateUser'); 
		$auth->addChild($pro, $updateUser);

		$viewUser = $auth->getPermission('viewUser'); 
		$auth->addChild($pro, $viewUser);	

		$crudWithoutIndex = $auth->getPermission('crudWithoutIndex'); 
		$auth->addChild($pro, $crudWithoutIndex);	

		$fullCrudPro = $auth->getPermission('fullCrudPro'); 
		$auth->addChild($pro, $fullCrudPro);	

		///////////////////////////////////////
		
		$admin = $auth->getRole('admin'); 
		$auth->addChild($admin, $pro);	
		
		$deleteUser = $auth->getPermission('deleteUser'); 
		$auth->addChild($admin, $deleteUser);
		
		$updatePassword = $auth->getPermission('updatePassword'); 
		$auth->addChild($admin, $updatePassword);
		
		$createUser = $auth->getPermission('createUser'); 
		$auth->addChild($admin, $createUser);
		
		$fullCrudAdmin = $auth->getPermission('fullCrudAdmin'); 
		$auth->addChild($admin, $fullCrudAdmin);

		

	}
		//////////////////////////////////////////////////
		
		public function actionAddfirstrule()
	{
		$auth = Yii::$app->authManager;
		
		$updateOwnUser = $auth->getPermission('updateOwnUser');
		$auth->remove($updateOwnUser);
		
		$rule = new \app\rbac\OwnUserRule;
		$auth->add($rule);
				
		$updateOwnUser->ruleName = $rule->name;		
		$auth->add($updateOwnUser);	
	}	
		////////////////////////////////////////
		public function actionAddsecondrule()
	{	
		$auth = Yii::$app->authManager;
		
		$updateOwnPassword = $auth->getPermission('updateOwnPassword');
		$auth->remove($updateOwnPassword);
		
		/////////////////////////////////////////////////עצרנו פה כי לא הצלחנו ליישם 2 הרשאות על אותו החוק למרות שבקורס הצלחנו. צריך לוודא שקיים updateOwnPassword
		$rule = new \app\rbac\OwnUserRule;
		$auth->add($rule);
				
		$updateOwnPassword->ruleName = $rule->name;		
		$auth->add($updateOwnPassword);	
	}	
	
}

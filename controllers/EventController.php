<?php

namespace app\controllers;

use Yii;
use app\models\Event;
use app\models\EventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
use app\models\className;


/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
      //access control
		if (!\Yii::$app->user->can('indexUser')) // all team members can watch events 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to watch events');
      $events = Event::find()->all();
        
      $tasks=[];  
      foreach ($events AS $eve){
      //Testing
      $event = new \yii2fullcalendar\models\Event();
      $event->id = $eve->id;
      $event->backgroundColor='#2F4F4F' ;
      $event->startEditable='true' ;
      $event->title = $eve->title;
      $event->description = $eve->description; 
	  $event->start = $eve->created_date; 
	  $event->end = $eve->endDate; 
	  //$event->textColor = $eve->classNumber;
	  //$event->url = $eve->;

	  
    //  $event->start = date('Y-m-d\Th:m:s\Z',strtotime('6am'));
      $tasks[] = $event;
		}
        return $this->render('index', [
            'events' => $tasks,
            ]);   
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
	 
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPro')) // only secretary's and principals can create events 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to create events');
		$model = new Event();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    
	public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

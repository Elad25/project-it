<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SecretarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Secretaries';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/yii/basic/images/secretary1.jpg" class="img-rounded" height="150" width="150" style="float: right;">
<div class="secretary-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Secretary', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
			<br><br><br>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
			[
				'attribute' => 'id',
				'label' => 'Secretary Name',
				'format' => 'raw',
				'value' => function($model){
					return $model->id0->fullname;  //////////Showing course name instead of course number.
				},
				//'filter'=>Html::dropDownList('CourseClassSearch[teacherId]', $teacher, $teachers, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			],
            'department',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

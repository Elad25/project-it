<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/yii/basic/images/students.jpg" class="img-rounded" height="120" width="324" style="float: right;">
<div class="student-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Student', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

<br><br>
    </p>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'firstname',
            'lastname',
            'email:email',
            //'grade',
			[
				'attribute' => 'grade',
				'label' => 'grade',
				'format' => 'raw',
				'filter'=>Html::dropDownList('StudentSearch[grade]', $grade, $grades, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			],
            // 'phone',
            // 'address',
            // 'notes:ntext',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Teacher;
use app\models\Student;

/* @var $this yii\web\View */
/* @var $model app\models\StudentTeacher */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-teacher-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'teacherId')->
	dropDownList(Teacher::getTeacher()) ?>  

    <?= $form->field($model, 'studentId')->dropDownList(Student::getStudent()) ?>  

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

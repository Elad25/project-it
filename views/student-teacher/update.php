<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StudentTeacher */

$this->title = 'Update Student Teacher: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Student Teachers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-teacher-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentTeacherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Student - Teachers';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/yii/basic/images/student-teacher.jpg" class="img-rounded" height="160" width="224" style="float: right;">
<div class="student-teacher-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Student - Teacher', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

<br><br><br><br>
		</p>
		
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           // 'id',
            //'teacherId',
			[
				'attribute' => 'teacherId',
				'label' => 'Teacher Name',
				'format' => 'raw',
				'value' => function($model){
					return $model->teacherName->fullname;  // Show teacher name instead of teacher id
				},
				'filter'=>Html::dropDownList('StudentTeacherSearch[teacherId]', $nameOfTeacher, $nameOfTeachers, ['class'=>'form-control']),
				
			],
            //'studentId',
			[
				'attribute' => 'studentId',
				'label' => 'Student Name',
				'format' => 'raw',
				'value' => function($model){
					return $model->student->fullname;  // Show teacher name instead of teacher id
				},
				'filter'=>Html::dropDownList('StudentTeacherSearch[studentId]', $nameOfStudent, $nameOfStudents, ['class'=>'form-control']),
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StudentTeacher */

$this->title = 'Create Student Teacher';
$this->params['breadcrumbs'][] = ['label' => 'Student Teachers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-teacher-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

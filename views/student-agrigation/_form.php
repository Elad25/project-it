<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Course;
use app\models\Classname;
use app\models\Student;

/* @var $this yii\web\View */
/* @var $model app\models\StudentAgrigation */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="Student-Course-Class-form">


    <?php $form = ActiveForm::begin(); ?>

	     <span class="glyphicon glyphicon-info-sign"></span><b> To create student - course - class you must create course - class before!</b> <span class="glyphicon glyphicon-info-sign"></span></p>
      
	
    <?= $form->field($model, 'studentId')->dropDownList(Student::getStudent()) ?>
	

    <?= $form->field($model, 'courseNumber')->dropDownList(course::getCourse()) ?>

    <?= $form->field($model, 'classNumber')->dropDownList(Classname::getClassnamenumber()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

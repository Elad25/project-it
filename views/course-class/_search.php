<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CourseClassSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-class-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'classNumber') ?>

    <?= $form->field($model, 'courseNumber') ?>

    <?= $form->field($model, 'duration') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'teacherId') ?>

    <?php // echo $form->field($model, 'id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Classname;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CourseClassSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Course - Class';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/yii/basic/images/course-class.jpg" class="img-rounded" height="160" width="224" style="float: right;">
<div class="course-class-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Course - Class', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
	
<br><br><br><br>
		</p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'classNumber',
			[
				'attribute' => 'classNumber',
				'label' => 'Class Location',
				'format' => 'raw',
				'value' => function($model){
					return $model->classNumber0->location;  //////////Showing class location instead of class id.
				},
				//'filter'=>Html::dropDownList('CourseClassSearch[teacherId]', $teacher, $teachers, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			],
            //'courseNumber',
			[
				'attribute' => 'courseNumber',
				'label' => 'Course Name',
				'format' => 'raw',
				'value' => function($model){
					return $model->courseNumber0->nameOfCourse;  //////////Showing course name instead of course number.
				},
				//'filter'=>Html::dropDownList('CourseClassSearch[teacherId]', $teacher, $teachers, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			],
            'duration',
            'date',
            //'teacherId',
			[
				'attribute' => 'teacherId',
				'label' => 'Teacher Name',
				'format' => 'raw',
				'value' => function($model){
					return $model->teacher->fullname;  //////////Showing teacher name instead of teacher id.
				},
				'filter'=>Html::dropDownList('CourseClassSearch[teacherId]', $teacher, $teachers, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			],			
            // 'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

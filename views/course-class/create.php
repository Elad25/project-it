<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CourseClass */

$this->title = 'Create Course Class';
$this->params['breadcrumbs'][] = ['label' => 'Course Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-class-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

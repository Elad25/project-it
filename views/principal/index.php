<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PrincipalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Principals';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/yii/basic/images/manager.png" class="img-rounded" height="150" width="115" style="float: right;">
<div class="principal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Principal', ['create'], ['class' => 'btn btn-primary']) ?>
</p>
<br><br><br>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
			[
				'attribute' => 'id',
				'label' => 'Principal Name',
				'format' => 'raw',
				'value' => function($model){
					return $model->id0->fullname;  //////////Showing course name instead of course number.
				},
				//'filter'=>Html::dropDownList('CourseClassSearch[teacherId]', $teacher, $teachers, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			],
            'level',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

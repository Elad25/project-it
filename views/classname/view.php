<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Classname */

$this->title = $model->classNumber;
$this->params['breadcrumbs'][] = ['label' => 'Classnames', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classname-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->classNumber], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->classNumber], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'classNumber',
            'maxOfStudents',
            'location:ntext',
        ],
    ]) ?>

</div>

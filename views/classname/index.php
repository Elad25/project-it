<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClassnameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Classnes';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/yii/basic/images/classroom.jpg" class="img-rounded" height="160" width="200" style="float: right;">
<div class="classname-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Classnes', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
	
<br><br><br>
		</p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'classNumber',
            'maxOfStudents',
            'location:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

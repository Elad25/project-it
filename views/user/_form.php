<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Userrole;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
	
	<?php if($model->isNewRecord){ ?>  <!-- show password field only if isNewRecord activate  --->
    <?= $form->field ($model, 'password')->passwordInput(['maxlength' => true]) ?>
	<?php }?>   <!-- end of isNewRecord condition   -->


    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

	<?/// status field is cancelled ?>

    <?= $form->field($model, 'userRole')-> dropDownList(Userrole::getUserRole()) ?>
 	
    <?= $form->field($model, 'role')-> dropDownList($roles) ?> 	
	

    <div class="form-group">
	
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

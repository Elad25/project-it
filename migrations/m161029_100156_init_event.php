<?php

use yii\db\Migration;

class m161029_100156_init_event extends Migration
{
    public function up()
    {
	   $this->createTable(
            'event',
            [
                'id' => 'pk',
                'title' => 'string',					
                'description' => 'string',						
                'created_date' => 'date',					
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
         $this->dropTable('event');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

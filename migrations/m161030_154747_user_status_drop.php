<?php

use yii\db\Migration;

class m161030_154747_user_status_drop extends Migration
{
    public function up()
    {
        $this->dropColumn('user','status');
    }

    public function down()
    {
	$this->addColumn('user','status','integer');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

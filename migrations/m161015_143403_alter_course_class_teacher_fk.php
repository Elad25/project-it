<?php

use yii\db\Migration;

class m161015_143403_alter_course_class_teacher_fk extends Migration
{
    public function up()
    {	
	$this->addColumn('course_class','teacherId','integer'); // We want to add foreign key 'id' from table 'teacher' to table 'course_class'
	
 // add foreign key for table `course_class`
        $this->addForeignKey(
            'fk-course_class-teacherId',// This is the fk => the table where i want the fk will be
            'course_class',// son table
            'teacherId', // son pk	
            'teacher', // father table
            'id', // father pk
            'CASCADE'
        );	
    }

    public function down()
    {
		
        $this->dropColumn('course_class','teacherId');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161024_165029_init_secretary extends Migration
{
   public function up()
    {
		   $this->createTable(
            'secretary',
            [
                'id' => 'pk',
                'department' => 'integer',					
            ],
            'ENGINE=InnoDB'
        );
		
		    // add foreign key for table `secretary`
        $this->addForeignKey(
            'fk-secretary-id',// This is the fk => the table where i want the fk will be
            'secretary',// son table
            'id', // son pk	
            'user', // father table
            'id', // father pk
            'CASCADE'
        );
    
    }

    public function down()
    {
         $this->dropTable('secretary');
		 
		 // drops foreign key for table `secretary`
        $this->dropForeignKey(
            'fk-secretary-id',
            'secretary'
        );
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161015_151734_init_role extends Migration
{
    public function up()
    {
		
				$this->createTable( // table roles of user member
            'userRole',
            [
                'roleId' => 'pk', 
                'roleName' => 'string'			
            ],
            'ENGINE=InnoDB'
        ); 

    }

    public function down()
    {
        $this->dropTable('userRole');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

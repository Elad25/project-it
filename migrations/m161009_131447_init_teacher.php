<?php

use yii\db\Migration;

class m161009_131447_init_teacher extends Migration
{
    public function up()
    {
	   $this->createTable(
            'teacher',
            [
                'id' => 'pk',
                'specialization' => 'string',					
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
         $this->dropTable('teacher');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

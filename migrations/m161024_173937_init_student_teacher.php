<?php

use yii\db\Migration;

class m161024_173937_init_student_teacher extends Migration
{
   public function up()
   {

	$this->createTable( //connection table between student & teacher
            'student_teacher',
            [
                'teacherId' => 'integer', // declaration attribute for them to be foreign key
                'studentId' => 'integer',				
                'id' => 'pk'				
            ],
            'ENGINE=InnoDB'
        ); 
	
	
		 // add foreign key for table `student_teacher`
        $this->addForeignKey(
            'fk-student_teacher-teacherId',// This is the fk => the table where i want the fk will be
            'student_teacher',// son table
            'teacherId', // son pk	
            'teacher', // father table
            'id', // father pk
            'CASCADE'
        );
		
		 // add foreign key for table `student_teacher`
        $this->addForeignKey(
            'fk-student_teacher-studentId',// This is the fk => the table where i want the fk will be
            'student_teacher',// son table
            'studentId', // son pk	
            'student', // father table
            'id', // father pk
            'CASCADE'
        );
    }
	

    public function down()
    {
         
		 
		  // drops foreign key for table `student_teacher`
        $this->dropForeignKey(
            'fk-student_teacher-studentId',
            'student_teacher'
        );
		
		  // drops foreign key for table `student_teacher`
        $this->dropForeignKey(
            'fk-student_teacher-teacherId',
            'student_teacher'
        );
		$this->dropTable('student_teacher');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

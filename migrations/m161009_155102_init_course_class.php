<?php

use yii\db\Migration;

class m161009_155102_init_course_class extends Migration
{
    public function up()
    {
		
		$this->createTable( //connection table between course & class
            'course_class',
            [
                'classNumber' => 'integer', // declaration attribute for them to be foreign key
                'courseNumber' => 'integer'				
            ],
            'ENGINE=InnoDB'
        ); 
	
	
		 // add foreign key for table `course_class`
        $this->addForeignKey(
            'fk-course_class-classNumber',// This is the fk => the table where i want the fk will be
            'course_class',// son table
            'classNumber', // son pk	
            'className', // father table
            'classNumber', // father pk
            'CASCADE'
        );
		
		 // add foreign key for table `course_class`
        $this->addForeignKey(
            'fk-course_class-courseNumber',// This is the fk => the table where i want the fk will be
            'course_class',// son table
            'courseNumber', // son pk	
            'course', // father table
            'courseNumber', // father pk
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropTable('course_class');
		 
		  // drops foreign key for table `course_class`
        $this->dropForeignKey(
            'fk-course_class-classNumber',
            'course_class'
        );
		
		  // drops foreign key for table `course_class`
        $this->dropForeignKey(
            'fk-course_class-courseNumber',
            'course_class'
        );
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

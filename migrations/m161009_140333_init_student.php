<?php

use yii\db\Migration;

class m161009_140333_init_student extends Migration
{
    public function up()
    {
	$this->createTable(
            'student',
            [
                'id' => 'pk',
                'firstname' => 'string',	
                'lastname' => 'string',	
				'email' => 'string',
				'grade' => 'string',
				'phone' => 'string',	
				'address' => 'string',	
                'notes' => 'text',
				'status' => 'integer',
				'created_at'=>'integer',
				'updated_at'=>'integer',
				'created_by'=>'integer',
				'updated_by'=>'integer'					
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
         $this->dropTable('student');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161024_063713_alter_classname extends Migration
{
     public function up()
    {
	$this->addColumn('classname','location','string');
	
    }

    public function down()
    {
        $this->dropColumn('classname','location');
      
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

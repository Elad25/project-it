<?php

use yii\db\Migration;

class m161009_160736_alter_course_class extends Migration
{
    public function up()
    {
	$this->addColumn('course_class','duration','integer');
	$this->addColumn('course_class','date','date');
    }

    public function down()
    {
        $this->dropColumn('course_class','duration');
        $this->dropColumn('course_class','date');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

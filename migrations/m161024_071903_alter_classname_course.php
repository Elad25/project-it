<?php

use yii\db\Migration;

class m161024_071903_alter_classname_course extends Migration
{
      public function up()
    {
	$this->addColumn('course_class','id','pk');
	
    }

    public function down()
    {
        $this->dropColumn('course_class','id');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161104_090858_alter_event extends Migration
{
    public function up()
    {	
	$this->addColumn('event','classNumber','integer'); // We want to add foreign key 'id' from table 'teacher' to table 'course_class'
	
 // add foreign key for table `course_class`
        $this->addForeignKey(
            'fk-event-classNumber',// This is the fk => the table where i want the fk will be
            'event',// son table
            'classNumber', // son pk	
            'classname', // father table
            'classNumber', // father pk
            'CASCADE'
        );	
    }
	
	
	public function down()
    {
		$this->dropForeignKey(
            'fk-event-classNumber',
            'event'
        );
		
        $this->dropColumn('event','classNumber');
		
		
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

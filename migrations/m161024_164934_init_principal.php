<?php

use yii\db\Migration;

class m161024_164934_init_principal extends Migration
{
    public function up()
    {
		   $this->createTable(
            'principal',
            [
                'id' => 'pk',
                'level' => 'integer',					
            ],
            'ENGINE=InnoDB'
        );
		
		    // add foreign key for table `principal`
        $this->addForeignKey(
            'fk-principal-id',// This is the fk => the table where i want the fk will be
            'principal',// son table
            'id', // son pk	
            'user', // father table
            'id', // father pk
            'CASCADE'
        );
    
    }

    public function down()
    {
         $this->dropTable('principal');
		 
		 // drops foreign key for table `principal`
        $this->dropForeignKey(
            'fk-principal-id',
            'principal'
        );
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

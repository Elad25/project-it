<?php

use yii\db\Migration;

class m161015_152520_alter_erd extends Migration
{
    public function up()
    {	
	$this->addColumn('user','userRole','integer'); // We want to add foreign key 'roleId' from table 'teamRole' to table 'team'
	
 // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user-userRole',// This is the fk => the table where i want the fk will be
            'user',// son table
            'userRole', // son pk	
            'userRole', // father table
            'roleId', // father pk
            'CASCADE'
        );	
    }

    public function down()
    {
			
		  // drops foreign key for table `team`
        $this->dropForeignKey(
            'fk-user-roleId',
            'user'
        );
		
		 $this->dropTable('userRole');	

        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161030_152946_student_status_drop extends Migration
{
    public function up()
    {
        $this->dropColumn('student','status');
    }

    public function down()
    {
	$this->addColumn('student','status','integer');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

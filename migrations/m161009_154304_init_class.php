<?php

use yii\db\Migration;

class m161009_154304_init_class extends Migration
{
    public function up()
    {
    $this->createTable(
            'className',
            [
                'classNumber' => 'pk',
                'maxOfStudents' => 'integer'
               					
            ],
            'ENGINE=InnoDB'
        );
		
		 $this->createTable(
            'course',
            [
                'courseNumber' => 'pk',
				'nameOfCourse' => 'string'
                
               					
            ],
            'ENGINE=InnoDB'
        );
    }
	
	

    public function down()
    {
         $this->dropTable('class');
         $this->dropTable('course');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

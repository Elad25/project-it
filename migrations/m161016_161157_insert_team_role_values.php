<?php

use yii\db\Migration;

class m161016_161157_insert_team_role_values extends Migration
{
    public function up()
    {
        $this->insert('userRole', [
            'roleName' => 'teacher',
            'roleId' => '1',
        ]);
        $this->insert('userRole', [
            'roleName' => 'principal',
            'roleId' => '2',
        ]);	
        $this->insert('userRole', [
            'roleName' => 'secretary',
             'roleId' => '3',
        ]);		
    }

    public function down()
    {
       $this->delete('userRole', ['roleName' => 'teacher']);
		$this->delete('userRole', ['roleName' => 'principal']);
		$this->delete('userRole', ['roleName' => 'secretary']);
    }	

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161009_163628_init_student_agrigation extends Migration
{
    public function up()
    {
	$this->createTable( //connection table between student & agrigation
            'student_agrigation',
            [
                'studentId' => 'integer', // declaration attribute for them to be foreign key
                'courseNumber' => 'integer',				
                'classNumber' => 'integer'				
            ],
            'ENGINE=InnoDB'
        ); 
	
		//////////////////////// 1 foreign key
	 // add foreign key for table `student_agrigation`
        $this->addForeignKey(
            'fk-student_agrigation-studentId',// This is the fk => the table where i want the fk will be
            'student_agrigation',// son table
            'studentId', // son pk	
            'student', // father table
            'id', // father pk
            'CASCADE'
        );

		//////////////////////// 2 foreign key	
	 // add foreign key for table `student_agrigation`
        $this->addForeignKey(
            'fk-student_agrigation-courseNumber',// This is the fk => the table where i want the fk will be
            'student_agrigation',// son table
            'courseNumber', // son pk	
            'course_class', // father table
            'courseNumber', // father pk
            'CASCADE'
        );		

		//////////////////////// 3 foreign key			
		 // add foreign key for table `student_agrigation`
        $this->addForeignKey(
            'fk-student_agrigation-classNumber',// This is the fk => the table where i want the fk will be
            'student_agrigation',// son table
            'classNumber', // son pk	
            'course_class', // father table
            'classNumber', // father pk
            'CASCADE'
        );		
    }

    public function down()
    {

		
			////////////////////// 1 foreign key drop
		  // drops foreign key for table `student_agrigation`
        $this->dropForeignKey(
            'fk-student_agrigation-studentId',
            'student_agrigation'
        );

			////////////////////// 2 foreign key drop
		  // drops foreign key for table `student_agrigation`
        $this->dropForeignKey(
            'fk-student_agrigation-courseNumber',
            'student_agrigation'
        );		

			////////////////////// 3 foreign key drop
		  // drops foreign key for table `student_agrigation`
        $this->dropForeignKey(
            'fk-student_agrigation-classNumber',
            'student_agrigation'
        );	

        $this->dropTable('student_agrigation');		
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
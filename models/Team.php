<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Query;


/**
 * This is the model class for table "team".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $notes
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $teamRole
 * @property string $username
 * @property string $password
 * @property string $auth_key
 *
 * @property Principal $principal
 * @property Secretary $secretary
 * @property Teacher $teacher
 * @property Teamrole $teamRole0
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notes'], 'string'],
            [['status', 'created_at', 'updated_at', 'created_by', 'updated_by', 'teamRole'], 'integer'],
            [['firstname', 'lastname', 'email', 'phone', 'address', 'username', 'password', 'auth_key'], 'string', 'max' => 255],
            [['teamRole'], 'exist', 'skipOnError' => true, 'targetClass' => Teamrole::className(), 'targetAttribute' => ['teamRole' => 'roleId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'notes' => 'Notes',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'teamRole' => 'Team Role',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrincipal()
    {
        return $this->hasOne(Principal::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecretary()
    {
        return $this->hasOne(Secretary::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamRole0()
    {
        return $this->hasOne(Teamrole::className(), ['roleId' => 'teamRole']);
    }
	
	public static function getTeames1()  
	{
		$allTeames = (new \yii\db\Query())
           ->select(['*'])
           ->from('team')
           ->where(['teamRole' => '1'])
           ->limit(10)
           ->all();
		$allTeamesArray = ArrayHelper::
					map($allTeames, 'id', 'username');
		return $allTeamesArray;						
	}
	
	public static function getTeames3()  
	{
		$allTeames = (new \yii\db\Query())
           ->select(['*'])
           ->from('team')
           ->where(['teamRole' => '3'])
           ->limit(10)
           ->all();
		$allTeamesArray = ArrayHelper::
					map($allTeames, 'id', 'username');
		return $allTeamesArray;						
	}
	
	public static function getTeames2()  
	{
	$allTeames = (new \yii\db\Query())
          ->select(['*'])
          ->from('team')
          ->where(['teamRole' => '2'])
          ->limit(10)
          ->all();
		$allTeamesArray = ArrayHelper::
					map($allTeames, 'id', 'username');
		return $allTeamesArray;						
	}
	
	
	
	
}

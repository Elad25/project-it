<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use app\models\User;


/**
 * This is the model class for table "userrole".
 *
 * @property integer $roleId
 * @property string $roleName
 *
 * @property User[] $users
 */
class Userrole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userrole';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['roleName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'roleId' => 'Role ID',
            'roleName' => 'Role Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['userRole' => 'roleId']);
    }
	
	
	public static function getUserRole() ///////////////////// this function is already exist
	{
		$allUserRole = self::find()->all();
		$allUserRoleArray = ArrayHelper::
					map($allUserRole, 'roleId', 'roleName');
		return $allUserRoleArray;						
	}
	////////////////////////////
		public static function getUserRoleWithAllRoles()   //////////////// new function - necessary for dropdown 
	{
		$allUserRole = self::getUserRole();
		$allUserRole[-1] = 'All Roles';
		$allUserRole = array_reverse ( $allUserRole, true );
		return $allUserRole;	
	}
	////////////////////////////
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $created_date
 * @property integer $classNumber
 * @property string $endDate
 *
 * @property Classname $classNumber0
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_date', 'endDate'], 'safe'],
            [['classNumber'], 'integer'],
            [['title', 'description'], 'string', 'max' => 255],
            [['classNumber'], 'exist', 'skipOnError' => true, 'targetClass' => Classname::className(), 'targetAttribute' => ['classNumber' => 'classNumber']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'created_date' => 'Start Date',
            'classNumber' => 'Location',
            'endDate' => 'End Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassNumber0()
    {
        return $this->hasOne(Classname::className(), ['classNumber' => 'classNumber']);
    }
}

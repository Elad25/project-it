<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "course".
 *
 * @property integer $courseNumber
 * @property string $nameOfCourse
 *
 * @property CourseClass[] $courseClasses
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nameOfCourse'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'courseNumber' => 'Course Number',
            'nameOfCourse' => 'Name Of Course',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseClasses()
    {
        return $this->hasMany(CourseClass::className(), ['courseNumber' => 'courseNumber']);
    }
	
	public static function getCourse()  // This function are already exist 
	{
		$allCourses = self::find()->all();
		$allCoursesArray = ArrayHelper::
					map($allCourses, 'courseNumber', 'nameOfCourse');
		return $allCoursesArray;						
	}
	

	
		public static function getCoursesWithAllCourses()
	{
		$allCourses = self::getCourse();
		$allCourses[-1] = 'All Courses';
		$allCourses = array_reverse ( $allCourses, true );
		return $allCourses;	
	}
	
	public function getCourseItem()
    {
        return $this->hasOne(Course::className(), ['nameOfCourse' => 'nameOfCourse']);
    }
	
	
}

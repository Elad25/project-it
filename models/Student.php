<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "student".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $grade
 * @property string $phone
 * @property string $address
 * @property string $notes
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property StudentAgrigation[] $studentAgrigations
 * @property StudentTeacher[] $studentTeachers
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notes'], 'string'],
            [[/*'status',*/ 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'], /// status is cancelled
            [['firstname', 'lastname', 'email', 'grade', 'phone', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	 
	 
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'First name',
            'lastname' => 'Last name',
            'email' => 'Email',
            'grade' => 'Grade',
            'phone' => 'Phone',
            'address' => 'Address',
            'notes' => 'Notes',
            //'status' => 'Status',  /// status is cancelled
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAgrigations()
    {
        return $this->hasMany(StudentAgrigation::className(), ['studentId' => 'id']);
    }
	
	public function getCreatedBy()
    {
		
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	
	
		public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
	
	
	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }
	
		 public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentTeachers()
    {
        return $this->hasMany(StudentTeacher::className(), ['studentId' => 'id']);
    }
	
	public static function getStudent()  // This function are already exist 
	{
		$allStudents = self::find()->all();
		$allStudentsArray = ArrayHelper::
					map($allStudents, 'id', 'firstname');
		return $allStudentsArray;						
	}
	
	////////////////////////////
	public static function getGrades()  // new function for grades
	{
		$allGrades = self::find()->all();
		$allGradesArray = ArrayHelper::
					map($allGrades, 'grade', 'grade');
		return $allGradesArray;	
	}
	
		public static function getGradesWithAllGrades()   //////////////// new function - necessary for dropdown 
	{
		$allGrades = self::getGrades();
		$allGrades[-1] = 'All Grades';
		$allGrades = array_reverse ( $allGrades, true );
		return $allGrades;	
	}
	////////////////////////////
		
}

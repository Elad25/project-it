<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentTeacher;

/**
 * StudentTeacherSearch represents the model behind the search form about `app\models\StudentTeacher`.
 */
class StudentTeacherSearch extends StudentTeacher
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacherId', 'studentId', 'id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StudentTeacher::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		$this->teacherId == -1 ? $this->teacherId = null : $this->teacherId;  //necessary for dropdown
		$this->studentId == -1 ? $this->studentId = null : $this->studentId;  //necessary for dropdown

        // grid filtering conditions
        $query->andFilterWhere([
            'teacherId' => $this->teacherId,
            'studentId' => $this->studentId,
            'id' => $this->id,
        ]);

        return $dataProvider;
    }
}

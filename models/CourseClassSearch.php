<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CourseClass;

/**
 * CourseClassSearch represents the model behind the search form about `app\models\CourseClass`.
 */
class CourseClassSearch extends CourseClass
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['classNumber', 'courseNumber', 'duration', 'teacherId', 'id'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseClass::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

			$this->teacherId == -1 ? $this->teacherId = null : $this->teacherId;  //necessary for dropdown		
		
        // grid filtering conditions
        $query->andFilterWhere([
            'classNumber' => $this->classNumber,
            'courseNumber' => $this->courseNumber,
            'duration' => $this->duration,
            'date' => $this->date,
            'teacherId' => $this->teacherId,
            'id' => $this->id,
        ]);

        return $dataProvider;
    }
}
